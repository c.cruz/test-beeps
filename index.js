const { EDGE_RISING, EDGE_BOTH, EDGE_NONE } = require('rpi-gpio')

const gpio = require('rpi-gpio').promise
const readline = require('readline')

const tickMs = 100

const DSPin = 16
const OEPin = 18
const writePin = 22
const clockPin = 24
const resetPin = 26

const wait = (ms) => new Promise((resolve) => setTimeout(resolve, ms))

const init = async () => {
  await Promise.all([
    gpio.setup(OEPin, gpio.DIR_OUT, EDGE_BOTH),
    gpio.setup(writePin, gpio.DIR_OUT, EDGE_BOTH),
    gpio.setup(resetPin, gpio.DIR_OUT, EDGE_BOTH),
    gpio.setup(clockPin, gpio.DIR_OUT, EDGE_BOTH),
    gpio.setup(DSPin, gpio.DIR_OUT, EDGE_BOTH),
  ])
  console.log('Pin setup finished.')

  await Promise.all([
    gpio.write(OEPin, false),
    gpio.write(writePin, false),
    gpio.write(DSPin, false),
    gpio.write(clockPin, false),
    gpio.write(resetPin, true),
  ])
  console.log('Pins initialized.')
}

const writeTick = async () => {
  await wait(tickMs)
  await gpio.write(writePin, true)

  await wait(tickMs)
  await Promise.all([gpio.write(clockPin, true), gpio.write(writePin, false)])

  await wait(tickMs)
  await gpio.write(writePin, true)

  await wait(tickMs)
  await Promise.all([gpio.write(clockPin, true), gpio.write(writePin, false)])

  await wait(tickMs)
  await gpio.write(clockPin, false)

  console.log('write ticked')
}

const tick = async (pin = clockPin) => {
  await wait(tickMs)
  await gpio.write(pin, true)

  await wait(tickMs)
  await gpio.write(pin, false)

  console.log('ticked')
}

const input = async () => {
  console.log('input signal...')
  await wait(tickMs)
  await gpio.write(DSPin, true)

  await tick()

  await wait(tickMs)
  await gpio.write(DSPin, false)
  console.log('done input signal.')
}

const reset = async () => {
  console.log('resetting...')
  await wait(tickMs)
  await gpio.write(resetPin, false)

  await tick(writePin)

  await wait(tickMs)
  await gpio.write(resetPin, true)
  console.log('done resetting.')
}

const question = (rl, question) =>
  new Promise((resolve) => {
    rl.question(question + '\n', resolve)
  })

const main = async () => {
  let running = true

  console.log('Starting...')

  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  })

  rl.on('SIGINT', async () => {
    running = false

    await reset()
    await wait(1000)
    await gpio.destroy()
    console.log('destroyed.')
    process.exit(0)
  })

  await init()

  await tick()

  await reset()

  await input()

  console.log('writing...')
  await writeTick()
  console.log('done writing')

  await wait(tickMs)
  await gpio.write(OEPin, true)

  await input()

  await wait(tickMs)
  await gpio.write(writePin, true)

  await wait(tickMs)
  await gpio.write(writePin, false)
  await gpio.write(OEPin, false)
  await gpio.write(clockPin, true)

  await wait(tickMs)
  await gpio.write(clockPin, false)

  while (running) {
    const command = await question(rl, 'Next action?')

    await executeCommand(command)
  }
}

const resolvePin = (name) => {
  switch (name) {
    case 'OE':
    case 'oe':
      return OEPin
    case 'w':
    case 'W':
    case 'write':
      return writePin
    case 'DS':
    case 'ds':
      return DSPin
    case 'C':
    case 'c':
    case 'clock':
      return clockPin
    case 'MR':
    case 'mr':
    case 'reset':
      return resetPin
  }
}

const executeCommand = async (command) => {
  if (command === 'reset') {
    await reset()
    return
  }

  if (command === 'cycle') {
    await tick(clockPin)
    await tick(writePin)
    return
  }

  const [pin, action] = command.split(' ')
  const pinNumber = resolvePin(pin) || clockPin

  if (action === 'on' || action == 1) {
    await gpio.write(pinNumber, true)
    console.log(`Turned ${pin} on.`)
    return
  }

  if (action === 'off' || action == 0) {
    await gpio.write(pinNumber, false)
    console.log(`Turned ${pin} off.`)
    return
  }

  if (action === 'tick' || action == 2) {
    await tick(pinNumber)
    console.log(`Ticked ${pin}.`)
    return
  }
}

main()
